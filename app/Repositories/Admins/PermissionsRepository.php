<?php

namespace App\Repositories\Admins;

use App\Models\Admins\Permission;
use App\Models\Admins\Role;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Auth;

class PermissionsRepository extends ModelRepository
{

    public function givePermission()
    {
        return Permission::_PERMISSION;
    }


    public function hasNavigation()
    {
        return true;
    }

    public function getName()
    {
        return "permissions";
    }

    public function getLabel()
    {
        return 'name';
    }

    public function getEndpoint()
    {
        return "permissions";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new Permission();
        return $model->newQuery();
    }

    public function getFields()
    {
        return [
            [
                "name"  => "name",
                "type"  => "text",
                "rules" => ["required"]
            ]
        ];
    }
}
