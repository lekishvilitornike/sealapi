<?php

namespace App\Models\Files;

use App\Models\Items\Item;
use App\Models\Model;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

/**
 * @property integer id
 * @property integer type_id
 * @property integer user_id
 * @property integer state
 * @property string url
 * @property string token
 * @property string server
 * @property string disk
 * @property array data
 * @property array qualities
 * @property array quality
 * @property FileType type
 */
class File extends Model
{
    const STATE_CREATED = 0;
    const STATE_UPLOADING = 1;
    const STATE_UPLOADED = 2;
    const STATE_ENCODING = 3;
    const STATE_ENCODED = 4;
    const STATE_FAILED = 5;

    protected $table = "files";

    protected $visible = ["id", "url", 'type_id'];
//
//    protected $with = ["type"];



    public function setQualitiesAttribute(array $qualities)
    {
        arsort($qualities);

        $this->attributes["qualities"] = implode(",", $qualities);
    }

    public function setDataAttribute(array $data)
    {
        $data = array_merge($this->data, $data);
        $this->attributes["data"] = json_encode($data);
    }


    public function getQualitiesAttribute($qualities)
    {
        return !is_null($qualities)
            ? explode(",", $qualities)
            : $this->type->qualities;
    }

    public function getQualityAttribute()
    {
        if (empty($this->qualities)) {
            return null;
        }

        return max($this->qualities);
    }

    public function getDataAttribute($data)
    {
        return (array)json_decode($data, true);
    }

    public function getUrlAttribute($url)
    {
        if (is_null($url)) {
            $url = config("filesystems.disks.{$this->disk}.url") . "/" . static::path($this->type, $this->id, $this->quality);
            return url($url);
        }
            
        return $url;
    }

    public static function path(FileType $type, $id, $quality = null)
    {

        $path = "{$type->name}s";
        $directory = "dir" . floor($id / 1000);

        $file = "file" . $id;
        if (!is_null(trim($quality))) {
            $file .= "_" . $quality;
        }

        return "{$path}/{$directory}/{$file}.{$type->ext}";
    }


    //Relations
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function item()
    {
        $relation = $this->belongsTo(Item::class, "type_id");

        return $relation;
    }

    public function type()
    {
        $relation = $this->belongsTo(FileType::class, "type_id");

        return $relation;
    }


}
