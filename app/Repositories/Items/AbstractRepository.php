<?php

namespace App\Repositories\Items;

use App\Models\Items\Item;
use App\Models\Items\CategoryType;
use App\Support\Resources\ModelRepository;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Elasticsearch\Client as Elastic;
use Elasticsearch\ClientBuilder as ElasticBuilder;


abstract class AbstractRepository extends ModelRepository
{
    public function __construct()
    {
        $this->fetching(function (Builder $query) {
            //dd($query);
            $query->where("type_id", $this->getTypeId());

            return $query;
        });
        $this->creating(function ($data) {

            /** @var User $user */
            $user = Auth::user();
            $data["user_id"] = $user->id;
            $data["cat_id"] = $data["cat_id"]["id"];
            $data["new"] = 1;
            $data["type_id"] = $this->getTypeId();
            //dd($data);
            return $data;
        });
        $this->saving(function ($data) {
            $this->relationsBeforeSave($data, "files");

            return $data;
        });




    }

    public function hasNavigation()
    {
        return true;
    }

    public function getName()
    {
        return $this->getEndpoint();
    }

    public function getLabel()
    {
        return "title";
    }

    abstract public function getTypeId();

    public function getEloquentQuery()
    {
        $model = new Item;
        return $model->newQuery();
    }

    public function getLoad()
    {
        return ["cat_type", "files","slides"];
    }

    public function getRelations()
    {
        return ["cat_type", "files","slides"];
    }

    public function availableFields($name = null)
    {
        $fields = [
            "main" => [
                [
                    "name" => "title",
                    "type" => "text",
                    "rules" => ["required"]
                ], [
                    "name" => "short_text",
                    "type" => "text",
                    "rules" => ["required"]
                ], [
                    "name" => "text",
                    "type" => "html",
                    "rules" => ["required"]
                ], [
                    "name" => "slug",
                    "type" => "text"
                ], [
                    "name" => "user_hide",
                    "type" => "boolean",
                    "rules" => ["required"]
                ],
                [
                    "name" => "main",
                    "type" => "boolean",
                ],
                [
                    "name" => "type_id",
                    "type" => "model:types",
                    "rules" => [],
                    "relation" => true,
                    "load" => false,
                ], [
                    "name" => "tags",
                    "type" => "text",
                    "rules" => ["required"],
                ],
                [
                    "name" => "tags",
                    "type" => "text",
                    "rules" => ["required"]
                ],[
                    "name" => "slides",
                    "type" => "table:item_slides",
                    "rules" => [],
                    "relation" => true,
                    "load" => true,
                ]

            ],
            "categories" => [
                [
                    "name" => "cat_id",
                    "type" => "model:categories",
                    "rules" => [],
                    "relation" => true,
                    "load" => false,
                ]
            ],
            "files" => [
                ["name" => "thumbnail",
                    "type" => "file:image_16x9",
                    "rules" => ["required"]
                ], [
                    "name" => "fb_thumbnail",
                    "type" => "file:image_fb_16x9",
                    "rules" => ["required"]
                ], [
                    "name" => "thumbnail_1x1",
                    "type" => "file:image_1x1",
                    "rules" => ["required"]
                ],
                [
                    "name" => "video",
                    "type" => "file:video",
                    "rules" => [],
                ]
            ]
        ];

        return is_null($name) ? $fields : $fields[$name];
    }

    private function getFieldsMap()
    {
        return collect(array_collapse($this->availableFields()))->keyBy("name");
    }

    protected function getFieldsByName(array $names)
    {
        $map = $this->getFieldsMap();
        $fields = [];
        foreach ($names as $name => $attr) {
            if (is_numeric($name)) {
                list($name, $attr) = [$attr, []];
            }
            if (!$map->has($name)) {
                throw new \Exception("undefined field name: $name");
            }
            $fields[] = array_merge($map[$name], $attr);
        }
        return $fields;
    }

    public function getColumns()
    {
        return [
//            'thumbnail',
            'title'
        ];
    }

    protected function relationsBeforeSave(array &$data, $property)
    {
        $fields = $this->availableFields($property);


        $order = 0;
        $relations = [];


        foreach ($fields as $field) {
            $key = $field["name"];
            $isArray = array_key_exists("is_array", $field) && $field["is_array"];

            if (array_key_exists($key, $data) && !is_null($data[$key])) {
                $_relations = $isArray ? $data[$key] : [$data[$key]];
                foreach ($_relations as $_relation) {
                    if (array_key_exists("id", $_relation)) {
                        if ($key != 'categories') {
                            $relations[] = [
                                "id" => $_relation["id"],
                                "pivot" => [
                                    "relation" => $key,
                                    "order" => ++$order
                                ]
                            ];
                        } else {
                            $relations[] = [
                                "id" => $_relation["id"],
                                "pivot" => [
                                    "order" => ++$order
                                ]

                            ];
                        }
                    }
                }
            }
            unset($data[$key]);
        }
        if (!empty($relations)) {
            $data[$property] = $relations;
        }

    }

    public function toArray(Model $item)
    {
        /** @var Item $item */
        $data = $item->toArray();

        $files = $this->relationsToArray($item->getRelation("files"), $this->availableFields("files"));
        //$cat = $this->relationsToArray($item->getRelation("cat_type"), $this->availableFields("categories"));

        unset($data["files"]);
        $data["cat_id"] = $data["cat_type"];

        unset($data["cat_type"]);

        $data = array_merge($data, $files);



        //dd($data);
        return $data;
    }


    protected function relationsToArray(Collection $relations, $fields)
    {
        if (is_null($relations)) return [];

        $relationsMap = $relations->groupBy('pivot.relation')->toArray();

        $array = [];

        foreach ($fields as $field) {
            $property = $field["name"];
            $isArray = array_key_exists("is_array", $field) && $field["is_array"];

            $value = array_key_exists($property, $relationsMap)
                ? $relationsMap[$property]
                : [];

            $array[$property] = $isArray ? $value : array_first($value, null, null);
        }


        return $array;
    }

    protected function relationToArray(Collection $relations, $key)
    {
        if (is_null($relations)) return [];
        $relationsMap = $relations->toArray();

        $array = [];

        foreach ($relationsMap as $field) {
            $array[$key] = $field;
        }

        return $array;
    }


}