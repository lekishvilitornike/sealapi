<?php

namespace App\Support\Resources;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /** @var Router */
    private $router;

    public function register()
    {
        $this->app->singleton(RepositoryManager::class, function () {
            return new RepositoryManager();
        });
    }

    public function boot()
    {
        $this->router = $this->app['router'];

        $this->router->group([
//            "middleware" => ["auth"],
            "prefix" => "api/v1/admin",
        ], function () {
            $repositories = $this->app->get(RepositoryManager::class)->getInstances();
            foreach ($repositories as $repository) {
                /** @var IRepository $repository */
                $this->registerRoutes($repository, $repository->getEndpoint());
            }
        });
    }

    private function registerRoutes(IRepository $repository, $url)
    {
        $this->router->get("$url/metadata", function (...$parameters) use ($repository) {
            $repository->setParameters($parameters);
            return $repository->getMetadata();
        });

        $this->router->get("$url", function (...$parameters) use ($repository) {
            $repository->setParameters($parameters);
            return $repository->getAll(request()->all());
        });

        $this->router->get("$url/suggestions", function (...$parameters) use ($repository) {
            $repository->setParameters($parameters);
            return $repository->getSuggestions(request()->all());
        });

        $this->router->get("$url/{id}", function (...$parameters) use ($repository) {
            $id = array_pop($parameters);
            $repository->setParameters($parameters);
            return $repository->getOne($id, request()->all());
        });

        $this->router->post("$url", function (...$parameters) use ($repository) {
            $repository->setParameters($parameters);
            return $repository->create(request()->all());
        });

        $this->router->put("$url/{id}", function (...$parameters) use ($repository) {
            $id = array_pop($parameters);
            $repository->setParameters($parameters);
            return $repository->update($id, request()->all());
        });

        $this->router->delete("$url/{id}", function (...$parameters) use ($repository) {
            $id = array_pop($parameters);
            $repository->setParameters($parameters);
            return $repository->destroy($id);
        });
    }
}
