<?php

namespace App\Models;

use App\Models\Admins\Role;
use App\Models\Admins\Site;
use App\Models\Admins\UserType;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Support\Collection;


/**
 * @property string site_id
 * @property Collection sites
 */
class User extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = "users";

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes["password"] = bcrypt($password);
    }

//    public function getSiteIdAttribute()

//    {
//        return 1;
//    }
    public function getSitesAttribute()
    {
        return Site::all();
    }

    // Relations

    public function site()
    {
        return $this->belongsTo(Site::class, "site_id");
    }


    public function role()
    {
        return $this->belongsTo(Role::class, "role_id");
    }

}
