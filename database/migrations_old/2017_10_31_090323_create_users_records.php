<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Database\Eloquent\Model::unguarded(function () {
            User::create([
                "name"     => "იაკო პატატიშვილი",
                "email"    => "iako@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "ლექსო მოაჭავარიანი",
                "email"    => "lexo@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "ოთარ ბურკიაშვილი",
                "email"    => "otar@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "პაატა ხარატიშვილი",
                "email"    => "paata@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "ბექა ჯიქურაშვილი",
                "email"    => "beka@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "ბადრი კობიაშვილი",
                "email"    => "badri@adjaramusic.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "გიორგი ნონიაშვილი",
                "email"    => "george_noniashvili@yahoo.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "გიორგი ლაცუზბაია",
                "email"    => "giorgilatsuzbaia@gmail.com",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "რეზი ხუნწელია",
                "email"    => "rezz@electronauts.ge",
                "password" => "qwerty123",
            ]);
            User::create([
                "name"     => "სალომე კვირიკაშვილი",
                "email"    => "salome@adjaramusic.com",
                "password" => "qwerty123",
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
