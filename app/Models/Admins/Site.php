<?php

namespace App\Models\Admins;

use App\Models\Model;

class Site extends Model
{
    const MUSIC = 1;
    const ELECTRO = 2;

    protected $table = "sites";

    public $timestamps = false;
}
