<?php

namespace App\Repositories\Items;

use App\Models\Items\CategoryType;
use App\Models\Items\ItemType;
use App\Models\Admins\Permission;
use App\Models\Admins\Role;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Model;

class CategoryRepository extends ModelRepository
{

    public function __construct()
    {
        $this->creating(function ($data) {
            $data["parent_id"] = $data["parent_id"]["id"];

            //dd($data);

            return $data;
        });

    }
    public function givePermission()
    {
        return Permission::_CATEGORY;
    }

    public function hasNavigation()
    {
        return true;
    }

    public function getName()
    {
        return "categories";
    }

    public function getLabel()
    {
        return 'name';
    }

    public function getEndpoint()
    {
        return "categories";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new CategoryType();
        return $model->newQuery();
    }

    public function getRelations()
    {
        return ["cat"];
    }

    public function getLoad()
    {
        return ["cat"];
    }

    public function toArray(Model $category)
    {
        /** @var CategoryType $category */
        $data = $category->toArray();

        if ($data["parent_id"]) {
            $data["parent_id"] = CategoryType::findOrFail($data["parent_id"]);
        }

        //dd($data,$parent);
//
        return $data;
    }

    public function getFields()
    {
        return [
            [
                "name" => "name",
                "type" => "text",
                "rules" => ["required"]
            ],
            [
                "name" => "status",
                "type" => "boolean",
                "rules" => ["required"]
            ],
            [
                "name" => "parent_id",
                "type" => "model:categories",
                "rules" => [],
                "relation" => true,
                "load" => false,
            ],
            [
                "name" => "order",
                "type" => "text",
                "rules" => ["required"]
            ]
        ];
    }
}
