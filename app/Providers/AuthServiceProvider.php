<?php

namespace App\Providers;

use App\Support\JwtGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app["auth"]->extend('jwt', function ($app, $name, array $config) {

            $provider = $app["auth"]->createUserProvider($config['provider']);

            $request = $app["request"];

            return new JwtGuard($provider, $request, $config);

        });
    }
}
