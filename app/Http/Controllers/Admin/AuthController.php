<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            "email"    => ["required", "email"],
            "password" => ["required", "min:4"]
        ]);


        if (Auth::attempt($request->only(["email", "password"]), $request->has('remember'))) {
            /** @var User $user */
            $user = Auth::user();
            $user->setAttribute("token", Auth::getAuthToken());
            $user->makeVisible("token");
            return $user;
        }

        return new JsonResponse(["email" => ["email or password is incorrect!"]], 422);
    }
}
