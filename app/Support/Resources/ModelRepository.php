<?php

namespace App\Support\Resources;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;

abstract class ModelRepository implements IRepository
{
    use RepositoryEventsTrait;

    protected $parameters = [];

    /** @return Builder|Relation */
    abstract public function getEloquentQuery();

    protected function log()
    {
        \DB::listen(function ($q) {
            echo $q->sql . "\n<br>";
            echo "bindings: (" . implode(",", $q->bindings) . ")\n<br>\n<br>\n<br>";
        });
    }

    public function getMetadata()
    {
        return [
            "name"     => $this->getName(),
            "endpoint" => $this->getEndpoint(),
            "key"      => $this->getKey(),
            "label"    => $this->getLabel(),
            "fields"   => $this->getFields(),
            "columns"  => $this->getColumns()
        ];
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }

    public function hasNavigation()
    {
        return true;
    }

    public function getKey()
    {
        return "id";
    }

    public function getLabel()
    {
        return "name";
    }

    public function getFields()
    {
        return [];
    }

    public function getColumns()
    {
        return [$this->getLabel()];
    }

    public function toArray(Model $model)
    {
        return $model->toArray();
    }

    public function getDefaultFetchingAllParameters()
    {
        return [];
    }

    private function validate(array $data)
    {
        $rules = $this->getRules();

        /** @var \Illuminate\Contracts\Validation\Factory $validatorFactory */
        $validatorFactory = app(Factory::class);

        /** @var \Illuminate\Validation\Validator $validator */
        $validator = $validatorFactory->make($data, $rules);

        if ($validator->fails()) {
            $response = new JsonResponse($validator->errors()->getMessages(), 422);
            throw new ValidationException($validator, $response);
        }
    }

    private function filter(array $data)
    {
        $attributes = [];
        foreach ($this->getFields() as $field) {
            $attributes[$field['name']] = array_get($data, $field['name'], null);
        }
        return $attributes;
    }

    protected function getLoad()
    {
        $fields = $this->getFields();
        $load = [];
        foreach ($fields as $field) {
            if (isset($field['load']) && $field['load']) {
                $load[] = $field["name"];
            }
        }
        return $load;
    }

    protected function getRelations()
    {
        $fields = $this->getFields();
        $relations = [];
        foreach ($fields as $field) {
            if (isset($field['relation']) && $field['relation']) {
                $relations[] = $field["name"];
            }
        }
//        dd($relations);
        return $relations;
    }

    private function getRules()
    {
        $fields = $this->getFields();
        $rules = [];
        foreach ($fields as $field) {
            if (isset($field['rules']) && $field['rules']) {
                $rules[$field["name"]] = $field["rules"];
            }
        }
        return $rules;
    }

    private function getQuery()
    {
        $query = $this->getEloquentQuery();

        $this->fireFetching($query instanceof Relation ? $query->getQuery() : $query);

        return $query;
    }

    private function findOrFail($id)
    {
        return $this->getQuery()->findOrFail($id);
    }

    public function getCount()
    {
        return $this->getQuery()->count();
    }

    public function getAll(array $params)
    {
        $params = array_merge($this->getDefaultFetchingAllParameters(), $params);

        $query = $this->getQuery();

        $count = $query->count();
        $perPage = (int)array_get($params, "count", 20);
        $currentPage = (int)array_get($params, "page", 1);
        $lastPage = ceil($count / $perPage);
        $orderColumn = array_get($params, "order", 'id');
        $orderDirection = array_get($params, "direction", 'desc');
        $data = $query
            ->with($this->getLoad())
            ->take($perPage)
            ->offset(($currentPage - 1) * $perPage)
            ->orderBy($orderColumn, $orderDirection)
            ->get()
            ->map(function ($model) {
                return $this->toArray($model);
            });

        return compact("count", "page", "currentPage", "lastPage", "perPage", "orderColumn", "orderDirection", "data");
    }

    public function getSuggestions(array $params)
    {
        $query = $this->getQuery();

        if (array_key_exists("keyword", $params)) {
            $query->where($this->getLabel(), "like", "%{$params['keyword']}%");
        }

        if (array_key_exists("except", $params)) {
            $query->whereNotIn($this->getKey(), $params['except']);
        }

        return $query
            ->take(8)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getOne($id, array $query)
    {
        $model = $this->findOrFail($id);

        $model->load($this->getLoad());

        return $this->toArray($model);
    }

    public function create(array $attributes)
    {
        /** @var Model $model */
        $model = $this->getEloquentQuery()->newModelInstance();

        return $this->doSave($model, $attributes);
    }

    public function update($id, array $attributes)
    {
        /** @var Model $model */
        $model = $this->findOrFail($id);

        return $this->doSave($model, $attributes);
    }

    private function doSave(Model $model, array $attributes): array
    {
        $this->validate($attributes);
        $attributes = $this->filter($attributes);
        $attributes = $this->fireCreating($attributes);
        $attributes = $this->fireSaving($attributes);
        $rel = $this->getEloquentQuery();
        $rel = $rel instanceof Relation ? $rel : null;

        /** @var Model $model */
        $model = ModelBuilder::buildAndSave($model, $attributes, $this->getRelations(), $rel);
        $this->fireCreated($model);
        $this->fireSaved($model);

        $this->pushNot($model);

        //dd($model["id"]);

        return $this->getOne($model->getKey(), []);
    }

    public function destroy($id)
    {
        /** @var Model $model */
        $model = $this->findOrFail($id);
        $this->fireDestroying($model);
        $model->delete();
        $this->fireDestroyed($model);
    }

    public function pushNot($model){
        $client = new Client();

        $res = $client->request('POST', 'https://api.foxpush.com/v1/campaigns/create/', [
            'headers' => [
                'FOXPUSH_DOMAIN' => 'front.primetime.ge',
                'FOXPUSH_TOKEN' => 'qkpEZ9LEdNH+edqaL6QpAw',
            ],
            'form_params' => [
                'name' => $model["title"],
                'title' => $model["title"],
                'url' => 'https://front.primetime.ge/detail/' . $model["id"],
                'message' => $model["short_text"]
            ]
        ]);
        return $res;
    }
}
