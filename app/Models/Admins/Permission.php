<?php

namespace App\Models\Admins;

use App\Models\Items\CategoryType;
use App\Models\Model;

class Permission extends Model
{
    const _ARTICLE = 1;
    const _VIDEO = 2;
    const _ROLE = 3;
    const _PERMISSION = 4;
    const _USERS = 5;
    const _CATEGORY = 6;
    const _LIVE = 7;
    const _ITEM_SLIDES = 8;
    
    protected $table = "permissions";

    public $timestamps = false;

}
