<?php

namespace App\Console\Commands;

use App\Models\User;
use Artisan;
use Illuminate\Console\Command;

class InitCommand extends Command
{
    protected $signature = 'init';

    protected $description = 'Init';

    public function handle()
    {
        \Eloquent::unguarded(function () {
            User::create([
                "name"     => "Admin",
                "email"    => "admin@adjara.com",
                "password" => "test123",
            ]);
        });

        Artisan::call('remove-local-files');
        Artisan::call('storage:link');
        Artisan::call('create-file-types');
    }
}
