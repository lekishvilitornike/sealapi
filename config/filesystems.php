<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],
        'image' => [
            'driver' => 'sftp',
            'host' => '85.117.37.152',
            'username' => 'static',
            'password' => 'breWs79KOpH',
            'root' => '/usr/share/nginx/static/files',
            "url" => 'https://staticmusic.adjara.com/files/',
            'directoryPerm' => 0755
        ],
        'video' => [
            'driver' => 'sftp',
            'host' => '85.118.108.9',
            'username' => 'upload',
            'password' => 'tons67PIx64^',
            'root' => '/video',
            "url" => 'https://storevideo1.adjara.com/',
            'directoryPerm' => 0755
        ],
        'video1' => [
            'driver' => 'sftp',
            'host' => '85.118.108.10',
            'username' => 'upload',
            'password' => 'lYsed94bircH$',
            'root' => '/video',
            "url" => 'https://storevideo1.adjara.com/',
            'directoryPerm' => 0755
        ],
        'local_upload' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads'),
            'url' => 'storage/uploads',
        ],
    ],
];
