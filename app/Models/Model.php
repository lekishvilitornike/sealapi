<?php

namespace App\Models;

use DateTime;
use DateTimeInterface;

/**
 * @property mixed id
 */
abstract class Model extends \Eloquent
{
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format(DateTime::ISO8601);
    }
}
