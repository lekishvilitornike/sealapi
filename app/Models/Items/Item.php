<?php

namespace App\Models\Items;


use App\Models\Files\File;
use App\Models\Model;
use App\Models\Items\ItemType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use App\Models\SliderSlide;


class Item extends Model
{
    //use SoftDeletes;

    protected $table = "news";
   // public $timestamps = false;
    //protected $hidden = ["cat_id"];

    protected $visible = [
        "id",
        "nid",
        "slug",
        "user_id",
        "cat_id",
        "user_hide",
        "main",
        "title",
        "new",
        "short_text",
        "created_at",
        "status",
        "cat_type",
        "text",
        "counter",
        "files",
        "img",
        "tags",
        "type",
        "author",
        "oldfiles",
        "main_news"
    ];


    public function cat_type() {
        return $this->belongsTo(CategoryType::class,"cat_id");
    }

//    public function file() {
//        return $this->hasMany(File::class,"nid");
//    }

    public function getCatIdAttribute($value)
    {
        return !is_numeric($value) ? unserialize($value)[0] : $value;
    }

    public function type() {
        return $this->belongsTo(ItemType::class,"type_id");
    }

    public function author(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function slides()
    {
        return $this->hasMany(SliderSlide::class, "item_id");
    }

    public function oldfiles()
    {
        return $this->hasMany(File::class, "nid");
    }

    public function files()
    {
        /** @var BelongsToMany $relation */
        $relation = $this->belongsToMany(File::class, "item_files", "item_id", "file_id");

        $relation->withPivot(["relation", "order"]);


        $relation->orderBy("item_files.order");


        return $relation;
    }

}
