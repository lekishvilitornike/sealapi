<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admins\UserType;
use App\Models\Files\FileType;
use App\Models\User;
use App\Support\Resources\IRepository;
use App\Support\Resources\RepositoryManager;
use Auth;

class SettingsController extends Controller
{
    /** @var User|null */
    protected $user;

    /** @var IRepository[] */
    protected $repositories;
    protected $locales;
    protected $defaultLocale;
    protected $contentLocales;
    protected $contentDefaultLocale;

    public function __construct()
    {
        $this->user = Auth::user();
        $this->repositories = app(RepositoryManager::class)->getInstances();
        $this->locales = ["ge"];
        $this->defaultLocale = "ge";
        $this->contentLocales = ["ge"];
        $this->contentDefaultLocale = "ge";

        $locale = in_array(request("locale"), $this->locales) ?
            request("locale") :
            $this->defaultLocale;

        app()->setLocale($locale);
    }

    public function index()
    {
        $data["locale"] = app()->getLocale();
        $data["locales"] = $this->locales;
        $data["defaultLocale"] = $this->defaultLocale;

        $data["contentLocales"] = $this->contentLocales;
        $data["contentDefaultLocale"] = $this->contentDefaultLocale;

        if ($this->user) {
        $data["user"] = $this->user;

        $data["fileConfigs"] = $this->getFileConfigs();

        $data['repositories'] = $this->getRepositories();

        $data["navigation"] = $this->getNavigation();
        }

        return $data;
    }

    private function getNavigation()
    {
        $navigation = [];

        foreach ($this->repositories as $repository) {
            /** @var IRepository $repository */
            if (!is_null($this->user->role_id) && !$this->checkPermissions($repository->givePermission())) {
                continue;
            }

            if ($repository->hasNavigation()) {
                $navigation[] = [
                    "name"  => $repository->getName(),
                    "link"  => ['/' . $repository->getName()],
                    "count" => $repository->getCount(),
                ];
            }
        }

        return $navigation;
    }

    private function getRepositories()
    {
        $repositories = [];
        foreach ($this->repositories as $repository) {
            /** @var IRepository $repository */

            if (!is_null($this->user->role_id) && !$this->checkPermissions($repository->givePermission())) {
                continue;
            }

            $repositories[$repository->getName()] = $repository->getMetadata();
        }
        return $repositories;
    }

    private function getFileConfigs()
    {
        return FileType::all()->keyBy("name");
    }

    private function checkPermissions($id)
    {
        $arr = [];

        $authUser = $this->user->load('role.permissions')->toArray();
        $userPermission = $authUser['role']['permissions'];
        foreach ($userPermission as $item) {
            $arr[] = $item['id'];
        }

        return in_array($id, $arr);
    }


}
