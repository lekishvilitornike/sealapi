<?php

namespace App\Support\Resources\FieldTypes;


use App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FieldManyToMany extends AbstractField
{
    public static function getType()
    {
        return "manyToMany";
    }

    public function pivot($pivotTable, $foreignId, $otherId)
    {

    }

    protected static function getModelRelation(Model $model, $relationMethodType, $relationMethodName)
    {
        if (method_exists($model, $relationMethodName) &&
            is_object($relation = $model->$relationMethodName()) &&
            $relation instanceof $relationMethodType) {
            return $relation;
        }

        throw new \LogicException("Relation not found");
    }

    public static function setAttribute(Model $model, FieldManyToMany $field, $value)
    {
//        $model->setAttribute($field->name, $value);

        $relation = static::getModelRelation($model, BelongsToMany::class, $field->name);

        /** @var BelongsToMany $relation */
        $model->saved(function () use ($relation, $value) {
            $relatedKeyName = $relation->getRelated()->getKeyName();
            $data = [];
            foreach ($value as $item) {
                if (array_key_exists($relatedKeyName, $item)) {
                    $data[$item[$relatedKeyName]] = array_get($item, 'pivot', []);
                }
            }
            $relation->sync($data);
        });
    }
}
