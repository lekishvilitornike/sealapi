<?php


namespace App\Traits;


use Exception;

/**
 * @property array attributes
 * @property array $dataAttributes
 */
trait DataAttributeTrait
{
    protected $attrExtracted = false;

    protected $attrDataName = "data";

    public function extract()
    {
        if (!$this->attrExtracted) {
            try {
                $properties = json_decode($this->attributes[$this->attrDataName], true);
                $this->dataAttributes = array_merge($this->dataAttributes, $properties);
            } catch (Exception $e) {

            }
            $this->attrExtracted = true;
        }
    }

    protected static function bootDataAttributeTrait()
    {
        static::saving(function ($model) {
            /** @var DataAttributeTrait $model */
            $json = json_encode($model->dataAttributes);
            $model->setAttribute($model->attrDataName, $json);
            return true;
        });
    }

    public function attributesToArray()
    {
        $this->extract();

        return array_merge(parent::attributesToArray(), $this->dataAttributes);
    }

    public function getAttribute($key)
    {

        if (array_key_exists($key, $this->dataAttributes)) {
            $this->extract();
            return $this->dataAttributes[$key];
        }

        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if (array_key_exists($key, $this->dataAttributes)) {
            $this->extract();
            $this->dataAttributes[$key] = $value;
            return;
        }

        parent::setAttribute($key, $value);
    }
}