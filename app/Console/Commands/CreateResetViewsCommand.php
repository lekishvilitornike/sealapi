<?php

namespace App\Console\Commands;

use App\Models\Items\Item;
use Illuminate\Console\Command;

class CreateResetViewsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset-views {arg}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function handle()
    {


        if ($this->argument('arg') == 'weekly') {

            Item::chunk(500, function($users) {

                foreach ($users as $user) {
                    $user->where('views_weekly', '!=', 0)->update(['views_weekly' => 0]);
                }

            });

        }


        if ($this->argument('arg') == 'monthly') {

            Item::chunk(500, function($users) {

                foreach ($users as $user) {
                    $user->where('views_monthly', '!=', 0)->update(['views_monthly' => 0]);
                }

            });

        }



    }
}
