<?php

namespace App\Repositories\Admins;

use App\Models\Admins\Permission;
use App\Models\Admins\Role;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Auth;

class RolesRepository extends ModelRepository
{

    public function __construct()
    {
        $this->saving(function ($data) {
            $this->relationsBeforeSave($data, "permissions");

            return $data;
        });
    }

    public function givePermission()
    {
        return Permission::_ROLE;
    }

    public function getLoad()
    {
        return ["permissions"];
    }

    public function getRelations()
    {
        return ["permissions"];
    }


    public function hasNavigation()
    {
        if (!is_null(Auth::user()->role_id)&& Auth::user()->role_id == 8)
        {

            return true;
        }

        return false;
    }

    public function getName()
    {
        return "roles";
    }

    public function getLabel()
    {
        return 'name';
    }

    public function getEndpoint()
    {
        return "roles";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new Role();

        if (is_null(Auth::user()->role_id))
        {

            return $model->newQuery();
        }

        return $model->newQuery()->where('id', '!=', 4);
    }

    public function getFields()
    {
        return [
            [
                "name"  => "name",
                "type"  => "text",
                "rules" => ["required"]
            ],  [
                "name"     => "permissions",
                "type"     => "models:permissions",
                "rules"    => [],
                "relation" => true,
                "load"     => false,
            ]
        ];
    }






    public function availableFields($name = null)
    {
        $fields = [
            "permissions"      => [
                 [
                    "name"     => "permissions",
                    "type"     => "models:permissions",
                    "rules"    => [],
                    "is_array" => true
                ]

            ],

        ];

        return is_null($name) ? $fields : $fields[$name];
    }





    protected function relationsBeforeSave(array &$data, $property)
    {
        $fields = $this->availableFields($property);


        $order = 0;
        $relations = [];


        foreach ($fields as $field) {
            $key = $field["name"];
            $isArray = array_key_exists("is_array", $field) && $field["is_array"];

            if (array_key_exists($key, $data) && !is_null($data[$key])) {
                $_relations = $isArray ? $data[$key] : [$data[$key]];
                foreach ($_relations as $_relation) {
                    if (array_key_exists("id", $_relation)) {
                            $relations[] = [
                                "id"    => $_relation["id"],
                            ];
                    }
                }
            }
            unset($data[$key]);
        }


        if (!empty($relations)) {
            $data[$property] = $relations;
        }

    }




}
