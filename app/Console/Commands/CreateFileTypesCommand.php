<?php

namespace App\Console\Commands;

use App\Models\Files\FileType;
use Illuminate\Console\Command;

class CreateFileTypesCommand extends Command
{
    protected $signature = 'create-file-types';

    protected $description = 'Command description';

    public function handle()
    {
        foreach (array_keys(config('encoding')) as $name) {
            FileType::firstOrCreate(['name' => $name]);
        }
    }
}
