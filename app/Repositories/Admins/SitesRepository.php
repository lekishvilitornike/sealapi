<?php

namespace App\Repositories\Admins;

use App\Models\Admins\Permission;
use App\Models\Admins\Site;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;

class SitesRepository extends ModelRepository
{

    public function givePermission()
    {
        return Permission::_SITE;
    }

    public function hasNavigation()
    {
        return false;
    }

    public function getName()
    {
        return "sites";
    }

    public function getLabel()
    {
        return 'name';
    }

    public function getEndpoint()
    {
        return "sites";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new Site();
        return $model->newQuery();
    }

    public function getFields()
    {
        return [
            [
                "name"  => "name",
                "type"  => "text",
                "rules" => ["required"]
            ]
        ];
    }
}
