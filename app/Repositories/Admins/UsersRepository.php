<?php

namespace App\Repositories\Admins;

use App\Models\Admins\Permission;
use App\Models\Admins\Site;
use App\Models\User;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Auth;

class UsersRepository extends ModelRepository
{

    public function __construct()
    {


        $this->creating(function ($data) {
            $data["role_id"] = $data["role"]["id"];

            return $data;
        });

    }

    public function givePermission()
    {
        return Permission::_USERS;
    }

    public function getLoad()
    {
        return ["role"];
    }

    public function getRelations()
    {
        return ["role"];
    }

    public function hasNavigation()
    {

        return true;
    }

    public function getName()
    {
        return "users";
    }

    public function getEndpoint()
    {
        return "users";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new User();
        return $model->newQuery()
            ->whereNotNull('role_id');
    }

    public function getColumns()
    {
        return [
            'name',
            'email'
        ];
    }


    public function getFields()
    {
        return [
            [
                "name"  => "name",
                "type"  => "text",
                "rules" => ["required"]
            ], [
                "name"  => "email",
                "type"  => "text",
                "rules" => ["required"]
            ], [
                "name"  => "password",
                "type"  => "text"
            ], [
                "name"     => "role",
                "type"     => "model:roles",
                "rules"    => ["required"],
                "relation" => true,
                "load"     => false,
            ]
        ];
    }


}
