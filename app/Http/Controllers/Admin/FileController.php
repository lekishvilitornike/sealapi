<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Files\File;
use App\Models\Files\FileType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FileController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            "type_id" => ["required", "integer", "exists:file_types,id"],
            "watermarkPosition" => ["string", "nullable"],
            "crop" => ["array", "nullable"],
            "crop.x" => ["required_with:crop", "numeric"],
            "crop.y" => ["required_with:crop", "numeric"],
            "crop.w" => ["required_with:crop", "numeric"],
        ]);

        /** @var User $user */
        $user = \Auth::user();
        /** @var FileType $type */
        $type = FileType::findOrFail($request->get("type_id"));


        /** @var FileType $type */
        $file = new File;
        $file->user_id = $user->id;
        $file->type_id = $type->id;
        $file->disk = $type->disk;
        $file->token = Str::random(40);
        $file->data = $request->only(["watermarkPosition", "crop","fb_title"]);
        $file->save();

        $response = $file->toArray();
        $response["server"] = "{$type->uploadUrl}?id={$file->id}&token={$file->token}";
        return $response;
    }
}
