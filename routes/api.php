<?php

Route::group([
    "namespace" => "Admin",
    "prefix"    => "api/v1/admin",
], function () {
    Route::resource("settings", "SettingsController");
    Route::post("auth/login", "AuthController@login");
    Route::post("files", "FileController@store");
});

Route::group([
    "namespace" => "Api",
    "prefix"    => "api/v1"
],
    function () {
        Route::get('/home', 'HomeController@index');
        Route::get('/item', 'ItemController@index');
        Route::get('/item/{id}', 'ItemController@show');
        Route::get('search', 'SearchController@index');
    });

Route::post("upload", "UploadController@handle");