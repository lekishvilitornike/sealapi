<?php

namespace App\Console\Commands;

use App\Models\Admins\Permission;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class CreatePermissionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function handle()
    {
        $types = [
            Permission::_NEWS           => "news",
            Permission::_ARTICLE        => "article",
            Permission::_TRACK          => "track",
            Permission::_EVENT_GEORGIAN => "georgian_event",
            Permission::_EVENT_FOREIGN  => "foreign_event",
            Permission::_LIST           => "list",
            Permission::_VIDEO          => "video",
            Permission::_LIST_ITEM      => "list_item",
            Permission::_AWARD          => "award",
            Permission::_GENRE          => "genre",
            Permission::_VENUE          => "venue",
            Permission::_ARTIST         => "artist",
            Permission::_LABEL          => "label",
            Permission::_CHANNEL        => "channel",
            Permission::_CITY           => "city",
            Permission::_FESTIVAL       => "festival",
            Permission::_ITEM_SLIDES    => "item_slides",
            Permission::_SLIDERS        => "sliders",
            Permission::_SLIDER_SLIDES  => "slider_slides",
            Permission::_CATEGORY       => "category",
            Permission::_FILE_TYPE      => "file_type",
            Permission::_ITEM_TYPE      => "item_type",
            Permission::_USERS          => "users",
            Permission::_ROLE           => "role",
            Permission::_PERMISSION     => "permission",
            Permission::_SITE           => "site",
            Permission::_KeyWord        => "keyword",
            Permission::_SEASON        => "season",
            Permission::_BUDGET        => "budget",
        ];

        Model::unguarded(function () use ($types) {
            foreach ($types as $id => $name) {
                Permission::updateOrCreate(compact("id"), compact("name"));
            }
        });
    }
}
