<?php

namespace App\Support\Resources;


use App\Repositories\Admins\PermissionsRepository;
use App\Repositories\Admins\RolesRepository;
use App\Repositories\Admins\UsersRepository;
use App\Repositories\Items\ArticleRepository;
use App\Repositories\Items\CategoryRepository;
use App\Repositories\Items\ItemTypeRepository;
use App\Repositories\Items\VideoRepository;
use App\Repositories\Items\ItemSlideRepository;
use App\Repositories\Items\LiveRepository;


class RepositoryManager
{
    protected $repositories = [];

    public function __construct()
    {
        $this->repositories[] = ArticleRepository::class;
        $this->repositories[] = VideoRepository::class;
        $this->repositories[] = ItemTypeRepository::class;
        $this->repositories[] = CategoryRepository::class;
        $this->repositories[] = ItemSlideRepository::class;
        $this->repositories[] = LiveRepository::class;

        $this->repositories[] = RolesRepository::class;

        $this->repositories[] = UsersRepository::class;

        $this->repositories[] = PermissionsRepository::class;
    }

    public function getInstances()
    {
        return array_map(function ($repositoryClassName) {
            return app($repositoryClassName);
        }, $this->repositories);
    }
}
