<?php

namespace App\Support\Resources;


interface IRepository
{
    public function hasNavigation();

    public function getName();

    public function getEndpoint();

    public function givePermission();

    public function getKey();

    public function getLabel();

    public function getFields();

    public function getColumns();

    public function getCount();

    public function setParameters(array $parameters);

    public function getAll(array $query);

    public function getOne($id, array $query);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function destroy($id);

    public function getSuggestions(array $query);

    public function getMetadata();
}
