<?php

namespace App\Repositories\Items;

use App\Models\Items\ItemType;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;

class ItemTypeRepository extends ModelRepository
{

    public function hasNavigation()
    {
        return true;
    }
    public function givePermission()
    {
    }

    public function getName()
    {
        return "types";
    }

    public function getLabel()
    {
        return 'name';
    }

    public function getEndpoint()
    {
        return "types";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new ItemType();
        return $model->newQuery();
    }

    public function getFields()
    {
        return [
            [
                "name"  => "name",
                "type"  => "text",
                "rules" => ["required"]
            ]
        ];
    }
}
