<?php

namespace App\Models;

use App\Models\Files\File;
use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderSlide extends Model
{
    use SoftDeletes;

    protected $table = 'slider_slides';



    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function image()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function video()
    {
        return $this->belongsTo(File::class, 'file_id');
    }
}
