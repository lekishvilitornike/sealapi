<?php

use App\Support\Translations\TranslationSchema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->string("title")->unique();
            $table->string('description', 4000)->nullable();
            $table->unsignedInteger('thumbnail_file_id')->nullable();
            $table->text('data')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('tag_types');
            $table->foreign('thumbnail_file_id')->references('id')->on('files');
        });

        TranslationSchema::create('tags', function (Blueprint $table) {
            $table->string("title");
            $table->string('description', 4000)->nullable();
        });

        Schema::create('tag_genre', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('genre_id');
            $table->primary(["tag_id", "genre_id"]);
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('genre_id')->references('id')->on('tags');
        });

        Schema::create('tag_slides', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('file_id');
            $table->unsignedInteger('order');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('file_id')->references('id')->on('files');
        });

        Artisan::call('create-tag-types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TranslationSchema::drop('tags');
        Schema::dropIfExists('tag_slides');
        Schema::dropIfExists('tag_genre');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('tag_types');
    }
}
