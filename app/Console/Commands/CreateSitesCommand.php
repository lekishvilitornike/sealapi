<?php

namespace App\Console\Commands;

use App\Models\Admins\Site;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class CreateSitesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-sites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function handle()
    {
        $sites = [
            Site::MUSIC   => "music",
            Site::ELECTRO => "electro",
        ];

        Model::unguarded(function () use ($sites) {
            foreach ($sites as $id => $name) {
                Site::updateOrCreate(compact("id"), compact("name"));
            }
        });
    }
}
