<?php

use App\Support\Translations\TranslationSchema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->string('title');
            $table->string('description', 4000)->nullable();
            $table->text('content')->nullable();
            $table->text('data')->nullable();
            $table->unsignedInteger('thumbnail_file_id')->nullable();
            $table->unsignedInteger('audio_file_id')->nullable();
            $table->unsignedInteger('video_file_id')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('main_page')->default(0);
            $table->unsignedInteger('main_section')->default(0);
            $table->unsignedInteger('is_active')->default(0);
            $table->unsignedInteger('favourite')->default(0);
            $table->unsignedInteger('page')->nullable();
            $table->unsignedInteger('site_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('item_types');
            $table->foreign('thumbnail_file_id')->references('id')->on('files');
            $table->foreign('video_file_id')->references('id')->on('files');
            $table->foreign('audio_file_id')->references('id')->on('files');
            $table->foreign('page')->references('id')->on('item_types');
        });

        TranslationSchema::create('items', function (Blueprint $table) {
            $table->string('title');
            $table->string('description', 4000)->nullable();
            $table->text('content')->nullable();
            $table->text('data')->nullable();
        });

        Schema::create('item_tag', function (Blueprint $table) {
            $table->string('relation', 16);
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('order')->nullable();
            $table->primary(["item_id", "tag_id"]);
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('tag_id')->references('id')->on('tags');
        });

        Schema::create('list_item', function (Blueprint $table) {
            $table->unsignedInteger('list_id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('order');
            $table->primary(["list_id", "item_id"]);
            $table->foreign('list_id')->references('id')->on('items');
            $table->foreign('item_id')->references('id')->on('items');
        });

        Artisan::call('create-item-types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TranslationSchema::drop('items');
        Schema::dropIfExists('list_item');
        Schema::dropIfExists('item_tag');
        Schema::dropIfExists('items');
        Schema::dropIfExists('item_types');
    }
}
