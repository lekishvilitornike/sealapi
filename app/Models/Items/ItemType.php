<?php

namespace App\Models\Items;

use App\Models\Model;


/**
 * @property mixed fields
 */
class ItemType extends Model
{
    const _ARTICLE = 1;
    const _VIDEO = 2;

    protected $table = "news_type";


    public $timestamps = false;


}
