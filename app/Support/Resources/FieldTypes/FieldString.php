<?php

namespace App\Support\Resources\FieldTypes;


class FieldString extends AbstractField
{
    public static function getType()
    {
        return "string";
    }

    public function richText()
    {
        $this->setComponent("rich-text");
        return $this;
    }
}
