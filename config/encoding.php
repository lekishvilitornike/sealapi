<?php


return [
    "image_16x9" => [
        "type" => "image",
        "ext" => "png",
        "accept" => "image/x-png,image/jpeg",
        "ratio" => 16 / 9,
        "qualities" => "1280,576,240",
        "upload" => env("IMAGE_UPLOAD_URL"),
        "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
    ],
        "image_fb_16x9" => [
            "type" => "image",
            "ext" => "png",
            "accept" => "image/x-png,image/jpeg",
            "ratio" => 16 / 9,
            "qualities" => "1280,576,240",
            "upload" => env("IMAGE_UPLOAD_URL"),
            "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
        ], "image_16x5" => [
            "type" => "image",
            "ext" => "png",
            "accept" => "image/x-png,image/jpeg",
            "ratio" => 16 / 5,
            "qualities" => "1110,360",
            "upload" => env("IMAGE_UPLOAD_URL"),
            "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
        ],
        "image_1x1" => [
            "type" => "image",
            "ext" => "png",
            "accept" => "image/x-png,image/jpeg",
            "qualities" => "512",
            "ratio" => 1 / 1,
            "upload" => env("IMAGE_UPLOAD_URL"),
            "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
        ],
        "image" => [
            "type" => "image",
            "ext" => "png",
            "accept" => "image/x-png,image/jpeg",
            "qualities" => "1280",
            "upload" => env("IMAGE_UPLOAD_URL"),
            "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
        ],
        "image_below" => [
            "type" => "image",
            "ext" => "png",
            "accept" => "image/x-png,image/jpeg",
            "ratio" => 153 / 37,
            "qualities" => "1530",
            "upload" => env("IMAGE_UPLOAD_URL"),
            "disk" => env("IMAGE_UPLOAD_DISK", "local_upload"),
        ],
        "video" => [
            "type" => "video",
            "ext" => "mp4",
            "upload" => env("MEDIA_UPLOAD_URL"),
            "disk" => env("MEDIA_UPLOAD_DISK", "local_upload"),
        ],
        "audio" => [
            "type" => "audio",
            "ext" => "mp3",
            "upload" => env("MEDIA_UPLOAD_URL"),
            "disk" => env("MEDIA_UPLOAD_DISK", "local_upload"),
        ],
    ];
