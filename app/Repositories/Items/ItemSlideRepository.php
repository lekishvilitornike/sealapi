<?php

namespace App\Repositories\Items;

use App\Models\Items\Item;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\Admins\Permission;
class ItemSlideRepository extends ModelRepository
{

    public function __construct()
    {

        $this->saving(function ($data) {
            if (isset($data['video']) && !is_null($data['video'])) {
                $data['file_type'] = 'video';
                $data['file_id'] = $data['video']['id'];
            }

            if (isset($data['image']) && !is_null($data['image'])) {
                $data['file_type'] = 'image';
                $data['file_id'] = $data['image']['id'];
            }

            return $data;
        });
    }

    public function givePermission()
    {
        return Permission::_ITEM_SLIDES;
    }

    public function hasNavigation()
    {
        return false;
    }

    public function getName()
    {
        return "item_slides";
    }

    public function getEndpoint()
    {
        return "item/{parentId}/slides";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        /** @var Item $model */
        $model = Item::findOrFail($this->parameters[0]);

        return $model->slides();
    }


    public function getColumns()
    {
        return [
            "order",
            "image",
            "title",
            "is_active"
        ];
    }

    public function getFields()
    {
        return [
            [
                "name"  => "title",
                "type"  => "text",
                "rules" => ["required"]
            ],
            [
                "name"      => "is_active",
                "type"      => "boolean",
                "rules"     => ["required"],
                "classList" => ["col-xs-1", "text-center"]
            ],
            [
                "name"     => "image",
                "type"     => "file:image_16x9",
                "rules"    => [],
                "relation" => true,
                "load"     => true
            ]
        ];
    }

}
