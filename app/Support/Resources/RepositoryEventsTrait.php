<?php

namespace App\Support\Resources;


use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait RepositoryEventsTrait
{
    private $events = [];

    private function registerEvent($name, Closure $closure)
    {
        if (!array_key_exists($name, $this->events)) {
            $this->events[$name] = [];
        }
        $this->events[$name][] = $closure;
    }

    private function fireEvent($name, $param)
    {
        if (array_key_exists($name, $this->events)) {
            foreach ($this->events[$name] as $event) {
                $param = call_user_func($event, $param);
            }
        }
        return $param;
    }

    //fire Events
    public function fireFetching(Builder $query): Builder
    {
        return $this->fireEvent("fetching", $query);
    }

    public function fireCreating(array $data): array
    {
        return $this->fireEvent("creating", $data);
    }

    public function fireUpdating(array $data): array
    {
        return $this->fireEvent("updating", $data);
    }

    public function fireSaving(array $data): array
    {
        return $this->fireEvent("saving", $data);
    }

    public function fireDestroying(Model $model)
    {
        return $this->fireEvent("destroying", $model);
    }

    //fire past events
    public function fireCreated(Model $model)
    {
        $this->fireEvent("created", $model);
    }

    public function fireUpdated(Model $model)
    {
        $this->fireEvent("updated", $model);
    }

    public function fireSaved(Model $model)
    {
        $this->fireEvent("saved", $model);
    }

    public function fireDestroyed(Model $model)
    {
        $this->fireEvent("destroyed", $model);
    }

    //Register Events
    public function fetching(Closure $closure)
    {
        $this->registerEvent("fetching", $closure);
    }

    public function creating(Closure $closure)
    {
        $this->registerEvent("creating", $closure);
    }

    public function created(Closure $closure)
    {
        $this->registerEvent("created", $closure);
    }

    public function updating(Closure $closure)
    {
        $this->registerEvent("updating", $closure);
    }

    public function updated(Closure $closure)
    {
        $this->registerEvent("updated", $closure);
    }

    public function saving(Closure $closure)
    {
        $this->registerEvent("saving", $closure);
    }

    public function saved(Closure $closure)
    {
        $this->registerEvent("saved", $closure);
    }

    public function destroying(Closure $closure)
    {
        $this->registerEvent("destroying", $closure);
    }

    public function destroyed(Closure $closure)
    {
        $this->registerEvent("destroyed", $closure);
    }
}