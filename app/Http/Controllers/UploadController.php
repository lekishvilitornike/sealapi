<?php

namespace App\Http\Controllers;


use App\Models\Files\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Image;

class UploadController extends Controller
{
    public function handle(Request $request)
    {
        /** @var File $file */
        $file = File::where($request->only(["token"]))->findOrFail($request->get("id"));

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = request()->file("file");
        $title = request()->file("fb_title");

        if ($file->type->type == "image") {
            $streams = $this->encodeImage($file, $uploadedFile);
        } else {
            $streams = $this->encodeMedia($file, $uploadedFile);
        }

        $this->upload($file, $streams);


        $file->qualities = array_keys($streams);
        $file->token = null;
        $file->state = File::STATE_ENCODED;


        $file->save();

        return $file;
    }

    private function upload(File $file, $streams)
    {

        foreach ($streams as $quality => $stream) {
            $path = File::path($file->type, $file->id, strlen(trim($quality)) > 0 ? $quality : null);
            Storage::disk($file->disk)->put($path, $stream, "public");

        }
    }

    private function encodeMedia(File $file, UploadedFile $uploadedFile)
    {
        return ["" => fopen($uploadedFile->getRealPath(), 'r+')];
    }

    private function encodeImage(File $file, UploadedFile $uploadedFile)
    {
        $streams = [];

        foreach ($file->type->qualities as $width) {
            $streams[$width] = $this->doEncodeImage($uploadedFile, array_merge($file->data, [
                "width" => $width,
                "ratio" => $file->type->ratio,
                "type" => $file->type->name,
                "fb_title" => $file->data["fb_title"]
            ]))->stream();

        }

        return $streams;
    }

    private function doEncodeImage(UploadedFile $uploadedFile, $config = [])
    {
        //dd($config);
        /** @var \Intervention\Image\Image $image */
        $image = Image::make($uploadedFile);

        
        $width = array_get($config, "width", 1920);
        $ratio = array_get($config, "ratio");
        $crop = array_get($config, "crop");

        if ($crop) {
            if ($ratio) {
                $crop['h'] = $crop['w'] / $ratio;
            }
            $image = $image->crop(
                (integer)$crop['w'],
                (integer)$crop['h'],
                (integer)$crop['x'],
                (integer)$crop['y']
            );
        }

        $c = min($width, $image->width()) / (double)$image->width();

        $w = (integer)($image->width() * $c);
        $h = (integer)($image->height() * $c);

        $image = $image->resize($w, $h);
        $name = array_get($config,"type");

        if (isset($config["watermarkPosition"])) {}
                if($name == "image_fb_16x9") {
                    $image = $this->imageAddWatermark($image, "top-left",$config["fb_title"]);
                }

        return $image;
    }

    private function imageAddWatermark(\Intervention\Image\Image $image, $position,$text)
    {
        //dd(resource_path("assets/img/watermark.png"));
        $watermark = Image::make(resource_path("assets/img/watermark.png"));

        $ratio = $watermark->getWidth() / $watermark->getHeight();

        $unit = (integer)($image->height() / 100);
        $height = $unit * 20;
        $padding = $unit * 10;

        $watermark->resize($ratio * $height, $height);

        $image->blur(20);
        $image->insert($watermark, $position, $padding, $padding);
        $image->text($text,$padding, $padding * 8, function($font) {
            $font->file(resource_path("assets/bpg-arial-caps-webfont.ttf"));
            $font->size(20);
            $font->color('#fdf6e3');
            $font->align('left');
            //$font->valign('middle');
        });

        return $image;
    }
}