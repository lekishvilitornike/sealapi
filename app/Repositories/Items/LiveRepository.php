<?php

namespace App\Repositories\Items;

use App\Models\Items\Live;
use App\Models\Items\ItemType;
use App\Support\Resources\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\Admins\Permission;
class LiveRepository extends ModelRepository
{

    public function hasNavigation()
    {
        return true;
    }
    
    public function givePermission()
    {
        return Permission::_LIVE;
    }
    public function getName()
    {
        return "lives";
    }


    public function getLabel()
    {
        return 'title';
    }

    public function getEndpoint()
    {
        return "lives";
    }

    /** @return Builder|Relation */
    public function getEloquentQuery()
    {
        $model = new Live();
        return $model->newQuery();
    }

    public function getFields()
    {
        return [
            [
                "name"  => "title",
                "type"  => "text",
                "rules" => ["required"]
            ],
            [
                "name"  => "status",
                "type"  => "boolean",
                "rules" => ["required"]
            ],
            [
                "name"  => "source",
                "type"  => "text",
                "rules" => ["required"]
            ]
        ];
    }
}
