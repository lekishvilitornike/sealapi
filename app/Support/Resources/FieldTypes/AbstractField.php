<?php

namespace App\Support\Resources\FieldTypes;


abstract class AbstractField
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $type;

    /** @var string */
    protected $component;

    /** @var array */
    protected $rules = [];

    /**
     * AbstractField constructor.
     * @param string $name
     * @param string $type
     */
    protected function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    abstract static public function getType();

    public static function make($name)
    {
        return new static($name, static::getType());
    }

    public function setComponent($component)
    {
        $this->component = $component;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function required()
    {
        $this->rules[] = "required";
    }

    public function toArray()
    {
        return [
            "name"      => $this->name,
            "type"      => $this->type,
            "component" => $this->component,
            "rules"     => $this->rules,
        ];
    }
}
