<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class RemoveLocalFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-local-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command Deletes Files from local storage.';

    public function handle()
    {
        File::cleanDirectory(\Storage::disk('local_upload')->path(''));
    }
}
