<?php

namespace App\Models\Files;

use App\Models\Model;

/**
 * @property integer id
 * @property string name
 *
 * @property mixed config
 * @property string type
 * @property string disk
 * @property string ext
 * @property string accept
 * @property string ratio
 * @property string width
 * @property array qualities
 *
 * @property string quality
 * @property string url
 * @property string uploadUrl
 */
class FileType extends Model
{
    protected $table = "file_types";
    public $timestamps = false;
    protected $appends = [
        "type",
        "ext",
        "accept",
        "ratio",
        "width",
        "qualities",
    ];

    protected $fillable = ["name"];


    public function getTypeAttribute()
    {
        return array_get($this->config, "type");
    }

    public function getExtAttribute()
    {
        return array_get($this->config, "ext");
    }

    public function getAcceptAttribute()
    {
        return array_get($this->config, "accept");
    }

    public function getRatioAttribute()
    {
        return (float)array_get($this->config, "ratio");
    }

    public function getWidthAttribute()
    {
        return (int)max($this->qualities);
    }

    public function getQualitiesAttribute()
    {
        return (array)array_map(function ($quality) {
            return (int)$quality;
        }, explode(",", array_get($this->config, "qualities")));
    }

    public function getDiskAttribute()
    {
        return array_get($this->config, "disk");
    }

    public function getUploadUrlAttribute()
    {
        return array_get($this->config, "upload") ?: url("upload");
    }

    public function getConfigAttribute()
    {
        return config("encoding.$this->name");
    }
}
