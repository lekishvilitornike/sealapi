<?php

namespace App\Support\Translations;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Schema;

class TranslationSchema
{
    private static function getTranslationTableName($tableName)
    {
        return str_singular("$tableName") . "_translations";
    }

    private static function getPropertyName($tableName)
    {
        return str_singular("$tableName") . "_id";
    }

    public static function create($tableName, Closure $closure)
    {
        Schema::create(static::getTranslationTableName($tableName), function (Blueprint $table) use ($tableName, $closure) {
            $property = static::getPropertyName($tableName);
            $table->increments('id');
            $table->string('locale');
            $table->unsignedInteger($property);
            $closure($table);
            $table->index(["locale"]);
            $table->unique(['locale', $property]);
            $table->foreign($property)->references('id')->on($tableName);
        });
    }

    public static function dropIfExists($tableName)
    {
        Schema::dropIfExists(static::getTranslationTableName($tableName));
    }
}