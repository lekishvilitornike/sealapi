<?php

namespace App\Models\Admins;

use App\Models\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Item
 * @property Collection roles
 */
class Role extends Model
{
    protected $table = "roles";


    public function permissions()
    {
        /** @var BelongsToMany $relation */
        return $this->belongsToMany(Permission::class, "role_permissions", "role_id", "permission_id");
    }

}
