<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function validate(Request $request,
                             array $rules,
                             array $messages = [],
                             array $customAttributes = [])
    {
        app(Factory::class)
            ->make($request->all(), $rules, $messages, $customAttributes)
            ->validate();

        return $request->only(collect($rules)->keys()->map(function ($rule) {
            return str_contains($rule, '.') ? explode('.', $rule)[0] : $rule;
        })->unique()->toArray());
    }
}
