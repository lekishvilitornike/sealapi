<?php

namespace App\Console\Commands;

use App\Models\Items\CategoryType;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class CreateItemTypesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-item-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function handle()
    {
        $types = [
            CategoryType::_NEWS           => "news",
            CategoryType::_ARTICLE        => "article",
            CategoryType::_TRACK          => "track",
            CategoryType::_EVENT_GEORGIAN => "georgian_event",
            CategoryType::_EVENT_FOREIGN  => "foreign_event",
            CategoryType::_LIST           => "list",
            CategoryType::_VIDEO          => "video",
            CategoryType::_LIST_ITEM      => "list_item",
            CategoryType::_AWARD          => "award",
            CategoryType::_GENRE          => "genre",
            CategoryType::_VENUE          => "venue",
            CategoryType::_ARTIST         => "artist",
            CategoryType::_LABEL          => "label",
            CategoryType::_CHANNEL        => "channel",
            CategoryType::_CITY           => "city",
            CategoryType::_FESTIVAL       => "festival",
            CategoryType::_KEYWORD        => "keyword",
            CategoryType::_SEASON         => "season",
            CategoryType::_BUDGET         => "budget",
        ];

        Model::unguarded(function () use ($types) {
            foreach ($types as $id => $name) {
                CategoryType::updateOrCreate(compact("id"), compact("name"));
            }
        });
    }
}
