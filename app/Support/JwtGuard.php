<?php

namespace App\Support;

use Exception;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class JwtGuard implements StatefulGuard
{
    protected $config;
    protected $loggedOut = false;
    protected $token;
    /**
     * @var UserProvider
     */
    private $provider;
    /**
     * @var Request
     */
    private $request;

    protected $query = 'token';

    protected $header = 'authorization';

    protected $method = 'bearer';

    /** @var Authenticatable|null */
    private $user = null;

    protected function getTokenFromRequest()
    {
        $header = $this->request->headers->get($this->header);

        if (starts_with(strtolower($header), $this->method)) {
            return trim(str_ireplace($this->method, '', $header));
        }

        if ($this->request->has($this->query)) {
            return $this->request->get($this->query);
        }

        return null;
    }


    /**
     * JwtGuard constructor.
     * @param UserProvider $userProvider
     * @param Request $request
     * @param $config
     */
    public function __construct(UserProvider $userProvider, Request $request, array $config)
    {
        $this->provider = $userProvider;
        $this->request = $request;
        $this->config = $config;
    }

    public function getAuthToken()
    {
        return $this->token;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return !is_null($this->user());
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->check();
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        if ($this->loggedOut) {
            return null;
        }

        if (!is_null($this->user)) {
            return $this->user;
        }

        $token = $this->getTokenFromRequest();

        if (!$token) {
            $this->loggedOut = true;
            return $this->user = null;
        }

        $secret = $this->config["secret"];

        $alg = ['HS256'];

        try {
            $payload = JWT::decode($token, $secret, (array)$alg);
        } catch (Exception $e) {
            $this->loggedOut = true;
            return $this->user = null;
        }
        $this->user = $this->provider->retrieveById($payload->sub);

        if (is_null($this->user)) {
            $this->loggedOut = true;
        }

        return $this->user;
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id()
    {
        return $this->user()->getAuthIdentifier();
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);

        return !is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }

    /**
     * Set the current user.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;

        $this->loggedOut = false;
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param  array $credentials
     * @param  bool $remember
     * @param  bool $login
     * @return bool
     * @throws Exception
     */
    public function attempt(array $credentials = [], $remember = false, $login = true)
    {
        $user = $this->provider->retrieveByCredentials($credentials);

        $status = !is_null($user) && $this->provider->validateCredentials($user, $credentials);

        if (!$status) {
            return false;
        }

        if ($login) {
            $this->login($user, $remember);
        }

        return true;
    }

    /**
     * Log a user into the application.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  bool $remember
     * @return void
     */
    public function login(Authenticatable $user, $remember = false)
    {
        $ttl = $this->config["ttl"];

        $payload = [
            "sub" => $user->getAuthIdentifier(),
            "iat" => time(),
            "nbf" => time(),
            "exp" => time() + $ttl,
            "jti" => $this->config["jti"]
        ];

        $secret = $this->config["secret"];

        $alg = Arr::get($this->config, "alg", "HS256");

        $token = JWT::encode($payload, $secret, $alg);

        $this->token = $token;

        $this->setUser($user);
    }

    /**
     * Log the given user ID into the application.
     *
     * @param  mixed $id
     * @param  bool $remember
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function loginUsingId($id, $remember = false)
    {
        $user = $this->provider->retrieveById($id);

        $this->login($user, $remember);
    }

    /**
     * Log a user into the application without sessions or cookies.
     *
     * @param  array $credentials
     * @return bool
     * @throws Exception
     */
    public function once(array $credentials = [])
    {
        throw new Exception("JwtGuard does Not implements");
    }

    /**
     * Log the given user ID into the application without sessions or cookies.
     *
     * @param  mixed $id
     * @return bool
     */
    public function onceUsingId($id)
    {
        $this->user = $this->provider->retrieveById($id);
        return $this->check();
    }

    /**
     * Determine if the user was authenticated via "remember me" cookie.
     * @return bool
     * @throws Exception
     */
    public function viaRemember()
    {
        throw new Exception("JwtGuard does Not implements");
    }

    /**
     * Log the user out of the application.
     *
     * @throws Exception
     */
    public function logout()
    {
        throw new Exception("JwtGuard does Not implements");
    }
}