<?php

namespace App\Support\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;


class ModelBuilder
{
    /** @var Model */
    private $model;

    /** @var array */
    private $relations;

    public function __construct(Model $model, array $relations)
    {
        $this->model = $model;
        $this->relations = $relations;
    }

    public static function buildAndSave(Model $model,
                                        array $attributes,
                                        array $relations,
                                        Relation $relation = null)
    {
        $builder = new static($model, $relations);
        $builder->fill($attributes, $relation);
        $builder->model->save();
        return $builder->model;
    }

    private function fill(array $attributes, Relation $relation = null)
    {
        $pivot = array_pull($attributes, "pivot", []);

        foreach ($attributes as $key => $value) {
            if (in_array($key, $this->relations)) {
                $this->setRelationAttribute($key, $value);
            } else {
                $this->setAttribute($key, $value);
            }
        }

        if (!is_null($relation)) {
            if ($relation instanceof BelongsToMany) {
                $this->model->saved(function (Model $model) use ($relation, $pivot) {
                    $id = $model->getKey();
                    $exists = $relation->newPivotStatementForId($id)->exists();
                    if ($exists) {
                        $relation->updateExistingPivot($id, $pivot);
                    } else {
                        $relation->attach($id, $pivot);
                    }
                });
            }
            if ($relation instanceof HasMany) {
                $this->setAttribute($relation->getForeignKeyName(), $relation->getParentKey());
            }
        }
    }

    private function setAttribute($key, $value)
    {
        $this->model->setAttribute($key, $value);
    }

    private function setRelationAttribute($key, $value)
    {
        /** @var Relation $relation */
        $relation = $this->getModelRelation($key);

        if ($relation instanceof BelongsTo) {
            $key = $relation->getOwnerKey();
            $foreignKey = $relation->getForeignKey();
            $value = is_array($value) && array_key_exists($key, $value)
                ? $value[$key]
                : null;
            $this->setAttribute($foreignKey, $value);
        }

        if ($relation instanceof BelongsToMany && is_array($value)) {
            /** @var BelongsToMany $relation */
            $this->model->saved(function () use ($relation, $value) {
                $relatedKeyName = $relation->getRelated()->getKeyName();
                $data = [];
                foreach ($value as $item) {
                    if (array_key_exists($relatedKeyName, $item)) {
                        $data[$item[$relatedKeyName]] = array_get($item, 'pivot', []);
                    }
                }
                if($this->model['type_id'] == 6) {
                    $relation->syncWithoutDetaching($data);
                } else {
                    $relation->sync($data);
                }
            });
        }
    }

    private function getModelRelation($name)
    {
        if (in_array($name, $this->relations) &&
            method_exists($this->model, $name) &&
            is_object($relation = $this->model->$name()) &&
            $relation instanceof Relation) {
            return $relation;
        }

        throw new \LogicException("Relation not found");
    }
}
