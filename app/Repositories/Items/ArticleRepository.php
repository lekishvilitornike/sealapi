<?php

namespace App\Repositories\Items;

use App\Models\Admins\Permission;
use App\Models\Items\CategoryType;
use App\Models\Items\ItemType;

class ArticleRepository extends AbstractRepository
{


    public function getEndpoint()
    {
        return "articles";
    }

    public function givePermission()
    {
        return Permission::_ARTICLE;
    }

    public function getTypeId()
    {
        return ItemType::_ARTICLE;
    }

    public function getFields()
    {
        return $this->getFieldsByName([
            "title",
            "short_text",
            "text",
            "cat_id",
            "user_hide",
            "main",
            "slug",
            "tags",
            "thumbnail",
            "fb_thumbnail",
            "video",
            "slides"
        ]);
    }
}



